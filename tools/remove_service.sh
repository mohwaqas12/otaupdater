#!/bin/sh
# OTA Service install 

#Envirnoment Variables 


if ! id | grep -q root; then 
	echo "must run as root or as sudo";
	exit 
fi 

SERVICE_NAME=ota-update.service
SERVICE_PATH=/lib/systemd/system

FILE_NAME=ota_update.sh
FILE_PATH=/usr/local/bin

systemctl disable  ota-update.service

rm -rf ${SERVICE_PATH}/${SERVICE_NAME}
rm -rf ${FILE_NAME} ${FILE_PATH}/${FILE_NAME}

systemctl daemon-reload 

systemctl list-unit-files ${SERVICE_NAME} --state=enabled 


