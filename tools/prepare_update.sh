#!/bin/sh
#This scripts prepares the ota update , copying all necessary files to STAGING_DIR
#############################
# SOFTWARE_DIR is the absolute software path that would be zipped and extracted during update process
# SET SOFTWARE_DIR in your ENVIRNOMENT PATH or uncomment the follow line and set its path here
# OR call this script with extra arg eg tools/prepare-update.sh "0.1" "/home/dir_to_install"
#SOFTWARE_DIR=test
#############################

if [ ! -z "$2" ]; then

 if [ ! -d "$2" ]; then
  echo "Dir $2 does not exists"
 fi

SOFTWARE_DIR="$2"

fi


STAGING_DIR=staging
SOFTWATE_TAR=software.tar.gz
SCRIPTS_DIR=src/scripts

RUN_DIR=update.d
rm -rf ${STAGING_DIR}

mkdir -p ${STAGING_DIR}/${RUN_DIR}

cp ${SCRIPTS_DIR}/*.sh ${STAGING_DIR}/${RUN_DIR}/

#Remove this one, will be copied if needed
rm  ${STAGING_DIR}/${RUN_DIR}/90-install-software.sh

# Copy version
echo -n "$1" >  ${STAGING_DIR}/version

#echo ${SOFTWARE_DIR}
if [ ! -z "${SOFTWARE_DIR}" ] && [ "${SOFTWARE_DIR}" != "" ]; then  
 tar zvcf ${STAGING_DIR}/${SOFTWATE_TAR} -C ${SOFTWARE_DIR}/ ./
 cp  ${SCRIPTS_DIR}/90-install-software.sh ${STAGING_DIR}/${RUN_DIR}/
fi

#Make scripts executable
chmod +x ${STAGING_DIR}/${RUN_DIR}/*.sh
exit 0
