#!/bin/sh
# OTA Service install 

#Envirnoment Variables 


if ! id | grep -q root; then 
	echo "must run as root or as sudo";
	exit 
fi 

SERVICE_NAME=ota-update.service
SERVICE_PATH=/lib/systemd/system

FILE_NAME=ota_update.sh
FILE_PATH=/usr/local/bin

cp src/${SERVICE_NAME} ${SERVICE_PATH}/${SERVICE_NAME}
cp src/${FILE_NAME} ${FILE_PATH}/${FILE_NAME}


systemctl daemon-reload 
#systemctl list-unit-files ${SERVICE_NAME} --state=enabled 
systemctl enable ota-update.service
systemctl list-unit-files ${SERVICE_NAME} --state=enabled 


