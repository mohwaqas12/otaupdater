#!/bin/sh
#This scripts creates the update file 
DEPLOY_DIR=deploy
STAGING_PATH=staging

VERSION=$(cat ${STAGING_PATH}/version)
UPDATE_FILE=ota.tar.gz

rm -rf ${STAGING_DIR}
mkdir -p ${DEPLOY_DIR}
tar zvcf ${DEPLOY_DIR}/${UPDATE_FILE} -C ${STAGING_PATH} .
