# OTA Updater
* Tool to create and run software updates

# Files 

* tools/prepare_update.sh verison_number software_dir to create an update folder into staging dir
* tools/create_update_file.sh to build ota update zip file
* tools/install_service.sh to install update service 
* tools/remove_service.sh to remove update service
* src includes all source cod
* staging : temporary directory
* deploy contains ota update zip file 


# Usage 

* cd otaupdater/
* tools/prepare_update.sh versionNo softwareDir , to create staging 
* tools/create_update_file.sh to create ota update file 
* copy deploy/ota.tar.gz to /var/spool/ota/ directory
* tools/install_service.sh to install ota service
* Run the update using "sudo systemctl start ota-update.service" or by rebooting the system
* Check the log using "journalctl -t ota-update"

# Todo
* Check and verify ota.tar.gz or software.tar.gz file inside it before extracting 
* Include more update scripts
* Have a way to report failed update
