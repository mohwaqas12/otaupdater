#!/bin/sh
# OTA Software Update Script
# Checks for new update file in ${UPDATE_DIR}
# Install update
# Continue 

#Envirnoment Variables 
if [ -z ${UPDATE_DIR} ]; then
	UPDATE_DIR=/var/spool/ota
fi

if [ -z ${UPDATE_FILE} ]; then
	UPDATE_FILE=ota.tar.gz
fi

if [ -z ${RUN_DIR} ]; then
	RUN_DIR=update.d
fi 

STAGING_DIR=staging

broadcast (){
	if [ "x${message}" != "x" ] ; then 
		echo "${message}"
	fi
}


cleanup_and_exit() {
	cd ${UPDATE_DIR}
	if [ -f ./${UPDATE_FILE} ]; then
		rm -f ./${UPDATE_FILE}
	fi


	# Remove the staging dir 
	if [ -d /${STAGING_DIR} ] && [ "$RUN_MODE" != "debug" ] ; then
		rm -rf ./${STAGING_DIR} 
	fi

	#TODO: Write success or failure 

	if [ "$RUN_MODE" = "bootup" ]; then
		if [ "$2" = "reboot" ]; then 
		message= "rebooting..."; broadcast
		systemctl reboot
		fi
	fi
	exit $1
}


check_for_update(){
	if [ ! -f "./${UPDATE_FILE}" ]; then 
		message="update file not present , contining "; broadcast
		exit 0
	fi
}


extract_update(){
	mkdir -p ./${STAGING_DIR}
	tar xzf ./${UPDATE_FILE} -C ${STAGING_DIR} -m
}

run_update(){

	cd ${STAGING_DIR}

	if [ ! -d ./${RUN_DIR} ]; then
		message="${RUN_DIR} directory not found, skip update"; broadcast
		cleanup_and_exit 1 reboot
	fi

	cd ${RUN_DIR}

	for filename in *.sh; do
	echo "--- $filename ---"
	./${filename}

	if [ $? -ne 0 ]; then
		echo "Script ${filename} failed"
		cleanup_and_exit 1 noreboot
	fi
	done
}

RUN_MODE=
while getopts ":bsd" opt; do
	case $opt in 
	b)
		RUN_MODE=bootup
		;;
	s)
		RUN_MODE=shutdown
		;;
	d)
		RUN_MODE=debug
		;;
	\?)
		echo "Invaled option -$OPTARG" >&2
		;;
	esac
done

if [ -z "${RUN_MODE}" ]; then
	message="Invalid args " ; broadcast
	exit 0
fi


#
if [ ! -d "${UPDATE_DIR}" ]; then 
	message="Update directory not present " ; broadcast
	exit 0
fi 

cd ${UPDATE_DIR}

check_for_update

extract_update

run_update

cleanup_and_exit 0 noreboot

