#!/bin/sh
# This script updates software version

VERSION_FILE="/usr/local/etc/version"

echo "Update Version number to "  $(cat ../version)
cat ../version > $VERSION_FILE

# Update version
exit 0
