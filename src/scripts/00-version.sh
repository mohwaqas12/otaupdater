#!/bin/sh
# This script checks version

. ./utils.sh

VERSION_FILE="/usr/local/etc/version"
#Checks version , exit if version is less then installed version
if [ -f "$VERSION_FILE" ]; then
 software_version=$(cat $VERSION_FILE)
fi
update_version=$(cat ../version)

if [ ! -z  $software_version ]; then
 echo "current version was $software_version"
 echo "update version is $update_version"

 check_version "$software_version" "$update_version"

  if [ $? -eq 1 ] ; then 
   echo "not doing anything"
   exit 1
  fi

else
 echo "current version not present"
 echo "new version $update_version avaiable"
fi

echo "Going to update"
exit 0
