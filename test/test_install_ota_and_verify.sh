#!/bin/sh

setup() {

echo "Stop and remove service"
sudo systemctl stop ota-update.service
sudo tools/remove_service.sh

echo "Install OTA service "
sudo tools/install_service.sh

echo "Remove old ota dir"
sudo rm -rf /var/spool/ota

echo "Create empty ota dir"
sudo mkdir -p /var/spool/ota

}

remove_old_version(){
echo "Remove old verison file"
sudo rm -rf /usr/local/etc/version

}
prepare_update(){
echo "Prepare update"
tools/prepare_update.sh $1 $2

echo "Create update file"
tools/create_update_file.sh

echo "Copy ota update file"
sudo cp deploy/ota.tar.gz /var/spool/ota/
}

prepare_test_software(){
echo "prepare test software"
mkdir -p /tmp/ota/software/usr/local/etc 
mkdir -p /tmp/ota/software/usr/local/bin
touch  /tmp/ota/software/usr/local/etc/testfile1
touch /tmp/ota/software/usr/local/bin/testbin1
}

remove_test_software(){
echo "remove test software"
rm -rf /tmp/ota
rm -rf /usr/local/etc/testfile1
rm -rf /usr/local/bin/testbin1
}

verify_test_software(){

if [ -f "/usr/local/etc/testfile1" ] && [ -f "/usr/local/bin/testbin1" ]; then
 logger -t "ota_update.sh" "verify test software PASS"
 else 
 logger -t "ota_update.sh" "verify test software FAIL"
fi

}

run_update(){
echo "Run update"
sudo systemctl start ota-update.service
}

teardown(){

echo "Remove service"
sudo systemctl stop ota-update.service
sudo tools/remove_service.sh

}

test_update_firsttime(){
logger -t "ota_update.sh" "Running test test_update_firsttime"
setup
remove_old_version
prepare_update "0.1.2"
run_update
teardown

}

test_update_new_version(){
logger -t "ota_update.sh" "Running test test_update_new_version"
setup
prepare_update "0.1.3"
run_update
teardown
}

test_update_old_version(){
logger -t "ota_update.sh" "Running test test_update_old_version"
setup
prepare_update "0.1.2"
run_update
teardown
}

test_update_include_software(){
logger -t "ota_update.sh" "Running test test_update_include_software"
setup
remove_old_version
prepare_test_software
prepare_update "0.1.2" "/tmp/ota/software"
run_update

# Verify 
verify_test_software 
remove_test_software
teardown

}


test_update_firsttime
test_update_new_version
test_update_old_version
test_update_include_software

remove_old_version
